var app = new Vue({
    el: '#vueapp',
    data: {
        title: 'Запрос на проверку',
        buttonSendData: 'Отправить запрос',
        buttonGetData: 'Вывести запросы',
        modalWindowTitle: 'Отправка данных',
        closeModalWindowBtn: 'Спасибо',
        data: {},
        columns: ['Номер запроса', 'Запрос', 'Результат запроса']
    },

    mounted: function () {
        console.log('Hello from Vue!')
    },

    methods: {

        isEmpty(str){
            return (str.length == 0 || !str.trim())
        },

        showModal(message, context){
            var bracketCheckContainer = $(context).closest(".brackets_check_container");

            //Check for opened modal windows before open one more
            var openedModalWindows = $(bracketCheckContainer).find(".modal.fade.show");
        
            if(openedModalWindows.length>0){
                closeModalWindow(context);
            }
        
            $(bracketCheckContainer).find(".modal-body").text(message);
            $(bracketCheckContainer).find(".modal.fade").modal();
            $(bracketCheckContainer).find(".modal.fade").addClass("show");

        },

     

        closeModalWindow(context){

            var bracketCheckContainer = $(context).closest(".brackets_check_container");
            $(bracketCheckContainer).find(".modal.fade").removeClass("show");
    
        },

         escapeHtml (data) {

            var entityMap = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#39;',
                '/': '&#x2F;',
                '`': '&#x60;',
                '=': '&#x3D;'
                };
        
            return String(data).replace(/[&<>"'`=\/]/g, function (s) {
                return entityMap[s];
            });
        
        },



        sendQuery(event){
            
            //Save the context
            var _this = event.target;
            var vueObj = this;
            //Find the root container
            var bracketCheckContainer = $(_this).closest(".brackets_check_container");
            var query = bracketCheckContainer.find(".queryInput").val();

            if(this.isEmpty(query)){
                this.showModal("Ошибка вы пытаетесь отправить пустое поле!", _this);
                return;
            }

            //Clean input
            bracketCheckContainer.find(".queryInput").val("");

            $.ajax({
                type: "POST",
                url: "server/checkData.php",
                data: `s=${query}`,
                success: function(data){
            
                    if(data.success == true){
                        vueObj.showModal("Данные корректны - проверка успешно пройдена", _this);
                    }else{
                        vueObj.showModal("Данные некорректны - проверка не пройдена", _this);
                    }

                },
                error: function(){
                    vueObj.showModal(`Возникла ошибка с отправкой данных ${query}`, _this);
                }
            }); 
        },

        getQuery(event){
            console.log("get Data");
            var _this = event.target;

            var vueObj = this;

             //Find the root container
            var bracketCheckContainer = $(_this).closest(".brackets_check_container");
            var queryResultContainer = bracketCheckContainer.find(".queryResult");

            $.ajax({
                type: "GET",
                url: "server/getResults.php",
                success: function(data){
                
                    if(data.length == 0 || data == undefined){
                        var errorText = "<div class='alert alert-danger' role='alert'>Данных в базе данных пока нет либо возникла непредвиденная ошибка - обратитесь к администратору</div>"
                        queryResultContainer.html(errorText);
                        return;
                    }

                    //Need in order to make correct output to table
                    //because backend gives only number result of Boolean
                    data.forEach(function(element, key) {

                        //Use symbol transform for special symbols
                        element.bracket_query = vueObj.escapeHtml(element.bracket_query);
                        element.bracket_result = Boolean(element.bracket_result);
                    });

                    vueObj.data = data;

                },

                error:function(){
                    vueObj.showModal("Ошибка получения данных - обратитесь к администратору", _this);
                    return;
                }
            }); 
        }
    }

   });