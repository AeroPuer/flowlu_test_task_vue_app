<?php

//Check for JSON Errors
function checkJsonErrors($json){
    if ($json === false) {
        //Check json error
        $json = json_encode(array("jsonError", json_last_error_msg()));
        if ($json === false) {
            // This should not happen, but we go all the way now:
            $json = '{"jsonError": "unknown"}';
        }
        // Set HTTP response status code to: 500 - Internal Server Error
        http_response_code(500);
    }

    return $json;
}

?>