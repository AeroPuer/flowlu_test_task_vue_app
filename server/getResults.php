<?php

//Include our class to insert and get data from SQLite Database
include_once("SQliteDB.php");
include_once("api.php");

$db = new SQliteDB('brackets_query_history.db', 'queries_table');

$selectedData = $db->get_data();

$dataResult = [];

while($fetchedArray = $selectedData->fetchArray()){
    $dataResult[] = $fetchedArray;
}

$json = json_encode($dataResult);
$outputJson = checkJsonErrors($json);

header("Content-type:application/json");

echo $outputJson;


?>

