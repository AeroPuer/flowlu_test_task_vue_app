<?php

//Create class to work with SQLite3
class SQliteDB extends SQLite3
{ 
    private $_dbFileName;
    private $_dbTableName;

    function __construct($dbFileName, $dbTableName)
    {
        $this->_dbFileName = $dbFileName;
        $this->_dbTableName = $dbTableName;

        $this->open($this->_dbFileName);
        $this->exec('CREATE TABLE IF NOT EXISTS '. $this->_dbTableName .' (bracket_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        bracket_query VARCHAR(150) NOT NULL,
        bracket_result BOOLEAN NOT NULL)');
    }

    public function insertData($bracketQueryInput, $bracketResultInput){
        
        $stmt = $this->prepare("INSERT INTO ". $this->_dbTableName ." (bracket_query, bracket_result) VALUES (:bracket_query, :bracket_result)");
        $stmt->bindParam(':bracket_query', $bracketQueryInput);
        $stmt->bindParam(':bracket_result', $bracketResultInput);
        $stmt->execute();
    }

    public function get_data(){

        $stmt = $this->prepare('SELECT * FROM '. $this->_dbTableName .' ORDER BY bracket_id DESC');
        return $stmt->execute();
    }
}
?>